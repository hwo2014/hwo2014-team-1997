agent = require "./agent"
state = require "./state"
shortest = require "./shortestpath"

class PathFinder extends agent.Agent
  act: (car, cars, track, callback) ->
    result = null
    newLane = @myPath?.getNextMove car.position.piecePosition.pieceIndex + 1, car.lane

    if newLane? and car.endLane != newLane.laneIndex and not isNaN(newLane.laneIndex)
      result = new state.State(null, [newLane.laneIndex], @LOW_PRIORITY=1)
      console.log "Requesting lane change from #{car.lane} to #{newLane.laneIndex}"
    super(car, track, callback, result)
  update: (track) ->
    @myPath = new shortest.ShortestPath track

exports.PathFinder = PathFinder
class Piece
  constructor: (data, lanes, @id) ->
    if data.switch?
      @switch = data.switch
    if data.radius?
      @radius = data.radius
      @angle = data.angle
      @absAngle = Math.abs @angle
      @passes = []
      @lengths = []
      @radii = []
      for lane in lanes
        #Compute radius
        radius = @radius
        if @angle < 0
          radius += lane.distanceFromCenter
        else
          radius -= lane.distanceFromCenter
        @radii.push radius

        #Compute arc length
        @lengths.push(2.0 * radius * Math.PI * (Math.abs(@angle)/360.0))
    if data.length?
      @lengths = data.length
  length: (lane=0) ->
    if @radius?
      return @lengths[lane]
    else
      return @lengths
  toAngularVelocity: (speed, lane, ticks = 1.0) ->
    if @radius?
      result = (((speed * ticks) / @length(lane)) * @absAngle) / ticks
    else
      result = 0.0
    result
  toLinearSpeed: (angularVelocity, lane, ticks = 1.0) ->
    if @radius?
      result = (((angularVelocity * ticks) / @absAngle) * @length(lane)) / ticks
    else
      result = 0.0
    result
  radiusOfLane: (lane) ->
    if @radius? then result = @radii[lane] else 0

class Track
  constructor: (@id, @name, pieces, @lanes) ->
    @pieces = (new Piece(p, @lanes, i) for p, i in pieces)

  validIndex: (index) -> index % @pieces.length

  curveDistance: (pieceIndex, laneIndex) ->
    distance = 0.0
    id = @validIndex pieceIndex
    piece = @pieces[id]
    if piece?.radius?
      if piece.curveTotal?
        distance = piece.curveTotal
      else
        distance = piece.length(laneIndex) + @curveDistance(pieceIndex + 1, laneIndex)
        piece.curveTotal = distance
    distance
  curveBefore: (pieceIndex, laneIndex, inPieceDistance=0) ->
    distance = 0.0
    id = @validIndex pieceIndex
    piece = @pieces[id]
    if piece?.radius?
      inPieceDistance = piece.length(laneIndex) if inPieceDistance == -1
      distance = inPieceDistance + @curveBefore id-1, laneIndex, -1
    distance

  curveAfter: (pieceIndex, laneIndex, inPieceDistance=0) ->
    distance = 0.0
    id = @validIndex pieceIndex
    piece = @pieces[id]
    if piece?.radius?
      distance = (piece.length(laneIndex) - inPieceDistance) + @curveDistance id+1, laneIndex
    distance

  straightDistance: (pieceIndex) ->
    distance = 0.0
    id = @validIndex pieceIndex
    piece = @pieces[id]
    if piece? and not piece.radius?
      if piece.straightTotal?
        distance = piece.straightTotal
      else
        distance = piece.length(0) + @straightDistance(id+1)
        piece.straightTotal = distance
    distance

  distanceTo: (fromPieceIndex, laneIndex, fromDistance, toPieceIndex) ->
    distance = 0.0
    id = @validIndex fromPieceIndex
    distance = -fromDistance
    while id != toPieceIndex
      piece = @pieces[id]
      distance += piece.length(laneIndex)
      id  = @validIndex(id+1)
    distance

exports.Track=Track

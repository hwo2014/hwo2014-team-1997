car = require "./car"
track = require "./track"
speed = require "./speeddaemon"
pathfinder = require "./pathfinder"
strategist = require "./strategist"
turbo = require "./turbo"
messages = require "./messages"
position = require "./position"
_js = require "underscore"

class Processor
  constructor: () ->
    @agents = [
      new speed.SpeedDaemon
      new pathfinder.PathFinder
      new strategist.Strategist
      new turbo.Turbo
    ]
    @track=null
    @car=null
    @gameState=0
  getResponse: (message) ->
    states = []
    switch message.msgType
      when 'carPositions'
        @tick = message.gameTick
        newPosition = new position.Position message.data
        car.update newPosition.of(car.color), @tick for car in @cars
        if @tick >= 0 and not @finished
          states = (agent.act(@car, @cars, @track, null) for agent in @agents)
      when "lapFinished"
        console.log "lapFinished"
      when "turboAvailable"
        console.log "turboAvailable"
        @car.turboAvailable = true
      when 'turboStart'
        color = message.data.color;
        car = _js.findWhere @cars, {color: color}
        car.turboActive = true
      when 'turboEnd'
        color = message.data.color;
        car = _js.findWhere @cars, {color: color}
        car.turboActive = false
      when 'join', 'createRace', 'joinRace'
        console.log 'Joined'
      when 'yourCar'
        @color = message.data.color
      when 'gameInit'
        console.log 'Init'
        trackData = message.data.race.track
        @track = new track.Track trackData.id, trackData.name, trackData.pieces, trackData.lanes
        @cars = (new car.Car(c, @track) for c in message.data.race.cars)
        console.log @cars
        @car = _js.findWhere @cars, {color: @color}
        agent.update @track for agent in @agents
        console.log "Track: %j", @track
        console.log "Color: #{@car.color}"
        console.log "Car Dimensions: %j", @car.dimensions
      when 'gameStart'
        console.log 'Race started'
        @tick = message.gameTick
        @gameStarted = true
      when 'crash'
        color = message.data.color;
        console.log "#{color} Crashes!"
        crashedCar = _js.findWhere @cars, {color: color}
        crashedCar.crash()
        #throw "end"
      when 'spawn'
        color = message.data.color;
        console.log '#{color} Respawns!'
        crashedCar = _js.findWhere @cars, {color: color}
        crashedCar.respawn()
      when 'gameEnd'
        console.log 'Race ended'
        console.log "Track: %j", @track
      when 'finish'
        color = message.data.color
        console.log "Finished: #{color}"
      when 'tournamentEnd'
        throw "end"

    command = @getMessage states


  getMessage: (states) ->
    result = messages.ping @tick
    if @car? and not @car.crashed
      nextState = null
      #get rid of nulls
      states = s for s in states if s?
      for state in states
        if not nextState? or state?.priority > nextState?.priority
          nextState = state

      if nextState? and @car?
        if nextState.throttle?
          result = messages.throttle nextState.throttle, @tick
          @car.throttle = nextState.throttle
        else if nextState.lanes?
          newLane = nextState.lanes[0]
          @car.requestedLane = newLane
          direction = if newLane > @car.lane then messages.RIGHT else messages.LEFT
          result = messages.switchLane direction, @tick
        else if nextState.turbo
          result = messages.turbo "Shazam!", @tick
          console.log "Using Turbo!!!"
          @car.turboAvailable = false
          @car.turboActive = true
    #console.log "Sending: %j", result
    result

exports.Processor = Processor

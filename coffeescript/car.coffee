metric = require "./metric"
_js = require "underscore"

class PassRecord
  constructor: (@lap, @color) ->
    @crashed = false
    @maxSlip = 0
    @maxAngularVelocity = 0
class Car
  constructor: (data, @track) ->
    @color = data.id.color
    @dimensions = data.dimensions
    @throttle=0.0
    @lane=null
    @angle = new metric.Metric
    @slip = new metric.Metric
    @maxThrottle=1
    @maxForce = 0.3
    @Force=0
    @minThrottle=0.0
    @position=null
    @lastPosition=null
    @speed=0
    @theta = 0
    @lastTheta = 0
    @distance = new metric.Metric
    @angularVelocity = 0
    @pass = new PassRecord(0, @color)
    @turboActive = false
  update: (position, tick) ->
    if not @finished?
      @lastPosition = @position
      @position = position
      @position.gameTick = tick
      ticks = @position.gameTick - @lastPosition?.gameTick
      @angle.update position.angle, ticks
      @slip.update Math.abs(@angle.value), ticks
      @lane = @position.piecePosition.lane.startLaneIndex
      @endLane = @position.piecePosition.lane.startLaneIndex
      lap = @position.piecePosition.lap
      pieceIndex = @position.piecePosition.pieceIndex
      lastPieceIndex = @lastPosition?.piecePosition.pieceIndex
      piece = @track.pieces[pieceIndex]
      if @lastPosition?.piecePosition.pieceIndex == pieceIndex
        @distance.update @position.piecePosition.inPieceDistance, ticks
        @speed = @distance.speed
        @acceleration = @distance.acceleration
        @angularVelocity = piece.toAngularVelocity(@speed, @lane, ticks)
        if piece.radius? and not @crashed
            @pass.maxAngularVelocity = Math.max @angularVelocity, @pass.maxAngularVelocity
            @pass.maxSlip = Math.max @slip.value, @pass.maxSlip
            @pass.lane = @lane
      else
        @distance.reset @position.piecePosition.inPieceDistance
        lastPiece = @track.pieces[lastPieceIndex]
        if lastPiece?.radius?
          lastPiece.passes.push @pass
          console.log "New pass: %j", @pass
          console.log "Passes: %j", lastPiece.passes
          if not @pass.crashed and lastPiece.speedLimit? and lastPiece.speedLimit.length > 0
            newSpeed = lastPiece.toLinearSpeed @pass.maxAngularVelocity, @pass.lane
            if newSpeed > lastPiece.speedLimit[@pass.lane]
              lastPiece.speedLimit = (lastPiece.toLinearSpeed newSpeed, j for lane, j in @track.lanes)

          @pass = new PassRecord @position.piecePosition.lap, @color

      console.log "Car (#{@color}): Lap: #{lap} Piece: #{pieceIndex} Lane: #{@lane} Slip: #{@slip.value.toFixed(2)} Speed: #{@speed.toFixed(2)} AV: #{@angularVelocity.toFixed(2)}"
    else
      console.log "Car (#{@color}): Finished"
  finish: () ->
    @finished = true
  crash: () ->
    console.log "Crash handler"
    @crashed = true
    @pass.crashed = true
    @pass.crashedAtDistance = @position.piecePosition.inPieceDistance
    pieceIndex = @position.piecePosition.pieceIndex
    before = @track.curveBefore pieceIndex, @lane, @pass.crashedAtDistance
    after = @track.curveAfter pieceIndex, @lane, @pass.crashedAtDistance

    #amount of whole curve completed before crashing
    curveCompleted = before/(before+after)
    console.log "Completed #{curveCompleted}% of the curve"
    piece = @track.pieces[pieceIndex]
    console.log "Passes: %j", piece.passes

    if piece? and piece.passes?
      minCrashed = Math.min.apply(null, p.maxAngularVelocity for p in piece.passes when p.crashed and p.maxAngularVelocity?)
      maxUncrashed = Math.max.apply(null, p.maxAngularVelocity for p in piece.passes when not p.crashed and p.maxAngularVelocity? and p.maxAngularVelocity < minCrashed)
      maxUncrashed = Math.max maxUncrashed, 1
    else
      maxUncrashed = 1
    newLimit = ((@angularVelocity - maxUncrashed) * curveCompleted) + maxUncrashed

    if newLimit > 0 and not @turboActive 
      console.log "New limit: #{newLimit}"

      if not piece.speedLimit?
        piece.speedLimit = (piece.toLinearSpeed newLimit, i for lane, i in @track.lanes)
      else
        console.log "Old limits: %j", piece.speedLimit
        piece.speedLimit = (Math.min(piece.toLinearSpeed(newLimit, i), piece.speedLimit[i]) for lane, i in @track.lanes)
        console.log "Updated: %j", piece.speedLimit

      #bootstrap other corners
      for otherPiece, i in @track.pieces
        if i != pieceIndex and otherPiece.radius? and (not otherPiece.speedLimit? or not otherPiece.passes? or otherPiece.passes?.length < 1)
          maxUncrashed = newLimit
          if otherPiece.passes?
            maxUncrashed = Math.max.apply(null, p.maxAngularVelocity for p in piece.passes when not p.crashed and p.maxAngularVelocity? and p.maxAngularVelocity < minCrashed)

          if not otherPiece.speedLimit?
            otherPiece.speedLimit = (otherPiece.toLinearSpeed Math.max(newLimit, maxUncrashed), j for lane, j in @track.lanes)
          else
            for limit, j in otherPiece.speedLimit
              newLaneLimit = otherPiece.toLinearSpeed Math.max(newLimit, maxUncrashed), j
              if newLaneLimit < limit
                otherPiece.speedLimit[j] = newLaneLimit

          console.log "Bootstrapping new speed limit on #{i}: %j", otherPiece.speedLimit



    

  respawn: () ->
    @crashed = false
exports.Car = Car

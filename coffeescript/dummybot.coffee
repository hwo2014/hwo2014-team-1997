net        = require "net"
JSONStream = require 'JSONStream'
# fs         = require 'fs'
messages   = require "./messages"


serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

# logfile = fs.createWriteStream("logs/" + Date.now() + "racelog.json")
# logfile.write "["

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort)

client = net.connect serverPort, serverHost, () ->
  send(messages.simpleJoin(botName, botKey))

send = (json) ->
  #logJSON json
  client.write JSON.stringify(json)
  client.write '\n'

#end = (finishCallback) ->
#  logfile.write "]"
#  logfile.end()
#  logfile.on 'finish', finishCallback if finishCallback?

#logJSON = (data) ->
#  logfile.write JSON.stringify(data)
#  logfile.write ',\n'

#log = (msg) ->
#  logfile.write "\"#{msg}\""
#  logfile.write ',\n'

jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (data) ->
  if data.msgType == 'carPositions'
    send {msgType: "throttle", data: 0.5}
  else
    if data.msgType == 'join'
      console.log 'Joined'
    else if data.msgType == 'gameStart'
      console.log 'Race started'
    else if data.msgType == 'gameEnd'
      console.log 'Race ended'
      process.exit(0)
    send {msgType: "ping"}

jsonStream.on 'error', ->
  console.log "disconnected"

class ShortestPath
  constructor: (@track) ->
    @FINISH="finish"
    @findShortestPath()
  findShortestPath: () ->
    @shortestPath=[]
    @distance = {}
    @intermediate = {}
    @nextMove={}

    adjacentEdges = (index) =>
      switch
        when index == 0 then [1]
        when index == (@track.lanes.length-1) then [index-1]
        else [index-1, index+1]

    getNextNodes = (pieceIndex, laneIndex) =>
      piece = @track.pieces[pieceIndex];
      lane = @track.lanes[laneIndex]
      
      nextNodes = {}
      length = piece.length laneIndex
      
      fromID = @getID(pieceIndex, laneIndex)
      toID = @getID(pieceIndex+1, laneIndex)
      
      nextNodes[toID] = true

      if not @distance[fromID]?
        @distance[fromID] = {}
      @distance[fromID][toID] = length

      if pieceIndex == (@track.pieces.length-1)
        nextNodes[@FINISH] = true
        @distance[fromID][@FINISH] = length

      if piece.switch
        for newIndex in adjacentEdges(laneIndex)
          toID = @getID(pieceIndex+1, newIndex)
          nextNodes[toID] = true
          @distance[fromID][toID] = length

      nextNodes

    allNodes = [{pieceIndex: @track.pieces.length, id: @FINISH}]
    for pieceIndex in [0..(@track.pieces.length-1)]
      for laneIndex in [0..(@track.lanes.length-1)]
        id = @getID(pieceIndex, laneIndex)
        @intermediate[id]=getNextNodes(pieceIndex, laneIndex)
        allNodes.push({pieceIndex: pieceIndex, laneIndex: laneIndex, id: id})
    
    for mid in allNodes
      for from in allNodes
        if from.pieceIndex < mid.pieceIndex
          for to in allNodes
            if mid.pieceIndex < to.pieceIndex
              d1 = @distance[from.id][mid.id]
              d2 = @distance[mid.id][to.id]
              d3 = @distance[from.id][to.id]
              if (d1? and d2?) and (not d3? or ((d1+d2) < d3))
                @distance[from.id][to.id]=d1+d2
                @intermediate[from.id][to.id]=mid.id

    findNext = (fromID, toID = @FINISH) =>
      if fromID != @FINISH
        intermediate = @intermediate[fromID][toID]
        if intermediate == true then toID
        else findNext(fromID, intermediate)

    for node in allNodes
      @nextMove[node.id] = findNext node.id
  getNextMove: (pieceIndex, laneIndex) ->
    [p, l] = @nextMove[@getID(pieceIndex, laneIndex)].split(":")
    result =
      pieceIndex: parseInt(p)
      laneIndex:  parseInt(l)

  getPath: (fromID, toID) ->
    intermediate = @intermediate[fromID][toID]

    if intermediate == true then " "
    else @getPath(fromID, intermediate) + intermediate + @getPath(intermediate, toID)

  getID: (pieceIndex, laneIndex) ->
    "#{pieceIndex%@track.pieces.length}:#{laneIndex}"

exports.ShortestPath = ShortestPath
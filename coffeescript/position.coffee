class Position
  constructor: (data) ->
    @cars = {}

    for car in data
      @cars[car.id.color] = car
  of: (color) -> @cars[color]
exports.Position = Position
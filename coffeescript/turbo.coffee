agent = require("./agent")
state = require("./state")

class Turbo extends agent.Agent
  act: (car, cars, track, callback) ->
    result = null
    if car.turboAvailable
      pieceIndex = car.position.piecePosition.pieceIndex
      lane = car.lane
      straightaway = track.straightDistance(pieceIndex)
      if straightaway > 600.0
        result = new state.State(null, null, @MED_PRIORITY)
        result.turbo = true
    super(car, track, callback, result)

exports.Turbo = Turbo
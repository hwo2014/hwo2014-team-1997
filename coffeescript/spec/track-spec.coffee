track = require "../track"

arcLength = (radius, angle) -> 2*Math.PI * radius * (Math.abs(angle)/360)

describe "The Track class", ()->
  testTrack = null
  gameInit = null
  beforeEach () ->
    gameInit =
      "msgType":"gameInit"
      "data":
        "race":
          "track":
            "id":"keimola"
            "name":"Keimola"
            "pieces":[
              {"length":100}
              {"length":100}
              {"length":100}
              {"length":100,"switch":true}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":200,"angle":22.5,"switch":true}
              {"length":100}
              {"length":100}
              {"radius":200,"angle":-22.5}
              {"length":100}
              {"length":100,"switch":true}
              {"radius":100,"angle":-45}
              {"radius":100,"angle":-45}
              {"radius":100,"angle":-45}
              {"radius":100,"angle":-45}
              {"length":100,"switch":true}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":200,"angle":22.5}
              {"radius":200,"angle":-22.5}
              {"length":100,"switch":true}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"length":62}
              {"radius":100,"angle":-45,"switch":true}
              {"radius":100,"angle":-45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"radius":100,"angle":45}
              {"length":100,"switch":true}
              {"length":100}
              {"length":100}
              {"length":100}
              {"length":90}
            ]
            "lanes":[
              {"distanceFromCenter":-10,"index":0}
              {"distanceFromCenter":10,"index":1}
            ]
            "startingPoint":
              "position":{"x":-300,"y":-44}
              "angle":90
          "cars":[
            "id":{"name":"B Team","color":"red"}
            "dimensions":{"length":40,"width":20,"guideFlagPosition":10}
          ]
          "raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}
      "gameId":"e1d12524-1029-41f4-b2af-95ec02d64253"
    trackData = gameInit.data.race.track
    testTrack = new track.Track trackData.id, trackData.name, trackData.pieces, trackData.lanes
  it "should consume track data", () ->
    expect(testTrack.name).toBe gameInit.data.race.track.name
    expect(testTrack.id).toBe gameInit.data.race.track.id
    expect(testTrack.lanes).toBe gameInit.data.race.track.lanes
    expect(testTrack.pieces.length).toBe gameInit.data.race.track.pieces.length
  it "should compute curve distance of 0 for a straightaway", () ->
    expect(testTrack.curveDistance(0,0)).toBe(0)
  it "should compute lane radius of right turns", () ->
    piece = testTrack.pieces[8]
    lane = testTrack.lanes[0]
    expect(piece.radiusOfLane 0).toBe (piece.radius - lane.distanceFromCenter)
    lane = testTrack.lanes[1]
    expect(piece.radiusOfLane 1).toBe (piece.radius - lane.distanceFromCenter)
  it "should compute lane radius of left turns", () ->
    piece = testTrack.pieces[14]
    lane = testTrack.lanes[0]
    expect(piece.radiusOfLane 0).toBe (piece.radius + lane.distanceFromCenter)
    lane = testTrack.lanes[1]
    expect(piece.radiusOfLane 1).toBe (piece.radius + lane.distanceFromCenter)
  it "should compute curve distances", () ->
    piece2 = testTrack.pieces[8]
    distance2 = arcLength piece2.radiusOfLane(0), piece2.angle
    expect(testTrack.curveDistance(8, 0)).toBe distance2

    piece1 = testTrack.pieces[7]
    distance1 = arcLength piece1.radiusOfLane(0), piece1.angle
    expect(testTrack.curveDistance(7, 0)).toBe(distance1 + distance2)
  it "should compute straightaway distances", () ->
    expect(testTrack.straightDistance(0)).toBe 400
    expect(testTrack.straightDistance(1)).toBe 300
    expect(testTrack.straightDistance(2)).toBe 200
    expect(testTrack.straightDistance(3)).toBe 100
  it "should compute straightaway distances wrapped to the beginning", () ->
    expect(testTrack.straightDistance(36)).toBe 790
    expect(testTrack.straightDistance(37)).toBe 690
    expect(testTrack.straightDistance(38)).toBe 590
  it "should compute lengths for straight pieces", () ->
    expect(testTrack.pieces[0].length()).toBe 100
    expect(testTrack.pieces[1].length()).toBe 100
    expect(testTrack.pieces[2].length()).toBe 100
  it "should compute lengths for right curved pieces", () ->
    piece1 = testTrack.pieces[4]
    expected = arcLength piece1.radiusOfLane(0), piece1.angle
    expect(testTrack.pieces[4].length()).toBe expected
    expect(testTrack.pieces[5].length()).toBe expected
    expect(testTrack.pieces[6].length()).toBe expected
  it "should compute lengths for left curved pieces", () ->
    piece1 = testTrack.pieces[14]
    expected = arcLength piece1.radiusOfLane(0), piece1.angle
    expect(testTrack.pieces[14].length()).toBe expected
    expect(testTrack.pieces[15].length()).toBe expected
    expect(testTrack.pieces[16].length()).toBe expected
  it "should compute curveBefore", () ->
    piece1 = testTrack.pieces[14]
    lanes = testTrack.lanes
    piece1Distance = arcLength piece1.radiusOfLane(0), piece1.angle
    piece2 = testTrack.pieces[15]
    piece2Distance = arcLength piece2.radiusOfLane(0), piece2.angle
    completed = piece2Distance /2
    expected = piece1Distance + completed
    expect(testTrack.curveBefore(15, 0, completed)).toBe expected
  it "should compute curveAfter", () ->
    piece1 = testTrack.pieces[7]
    lanes = testTrack.lanes
    piece1Distance = arcLength piece1.radiusOfLane(0), piece1.angle
    piece2 = testTrack.pieces[8]
    piece2Distance = arcLength piece2.radiusOfLane(0), piece2.angle
    completed = piece1Distance /2
    expected = piece2Distance + completed
    expect(testTrack.curveAfter(7, 0, completed)).toBe expected
  it "should compute distances", () ->
    expected = 100
    expect(testTrack.distanceTo(0, 0, 0, 1)).toBe expected
    expected = 200
    expect(testTrack.distanceTo(0, 0, 0, 2)).toBe expected
    expected = 150
    expect(testTrack.distanceTo(0, 0, 50, 2)).toBe expected
    expected = 250
    expect(testTrack.distanceTo(0, 0, 50, 3)).toBe expected
    expected = 450
    expect(testTrack.distanceTo(38, 0, 40, 3)).toBe expected


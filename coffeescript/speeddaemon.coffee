agent = require("./agent")
state = require("./state")

class SpeedDaemon extends agent.Agent
  constructor: () ->
    @limited = undefined
    super()

  act: (car, cars, track, callback) ->
    newThrottle = car.throttle
    result = null
    #piece = @track.pieces[ car.position.piecePosition.pieceIndex + 1]
    pieceIndex = car.position.piecePosition.pieceIndex
    currentpiece = track.pieces[pieceIndex]
    lane = track.lanes[car.lane]

    if not @deceleration? and car.distance.speed > 0
      newThrottle = 0
      if car.distance.lastSpeed > car.distance.speed
        @deceleration = car.distance.speed/car.distance.lastSpeed
        console.log "Deceleration is #{@deceleration}"
        newThrottle = car.maxThrottle
    else
      newThrottle = car.maxThrottle
      @limited = undefined if @limited? and @limited.id == track.validIndex(pieceIndex-1)

      @limited = @findNextSpeedLimit(car, track, pieceIndex)

      if @limited?
        distance = track.distanceTo(pieceIndex, car.lane, car.distance.value, @limited.id)
        speedLimit = @limited.speedLimit[car.lane]
        console.log "Found limit at #{@limited.id} of #{speedLimit} in #{distance}"

        slowingDistance = @getSlowingDistance(car.distance.speed, speedLimit)
        console.log "It'll take #{slowingDistance} to slow"

        #Approaching slowing distance
        if slowingDistance >= distance
          if car.distance.speed < speedLimit
            newThrottle = car.maxThrottle
          else if pieceIndex == @limited.id and car.slip.speed < 1
            newThrottle += 0.05
          else
            newThrottle = 0
      else
        newThrottle = car.maxThrottle

      if car.slip.value > 20 and car.slip.speed > 1
        newThrottle  *= ((60-car.slip.value) / 60)

      #Constrain throttle...
      newThrottle = Math.max(newThrottle, car.minThrottle)
      newThrottle = Math.min(newThrottle, car.maxThrottle)

    console.log "Throttle is  #{newThrottle}"

    if (newThrottle != car.throttle or car.speed.value == 0.0 or car.position.gameTick % 2 == 0) and not car.crashed
      result = new state.State(newThrottle, null, priority=@MED_PRIORITY)

    super(car, track, callback, result)

  findNextSpeedLimit: (car, track, pieceIndex) ->
    result = undefined
    lookAhead = if car.turboActive then 8 else 4
    for i in [pieceIndex+1..(pieceIndex+lookAhead)]
      if not result?
        nextPiece = track.pieces[track.validIndex i]
        if nextPiece.speedLimit? and nextPiece.speedLimit.length > 0
          result = nextPiece
    result

  getSlowingDistance: (currentSpeed, targetSpeed) ->
    slowingDistance = 0
    while currentSpeed > targetSpeed
      slowingDistance += currentSpeed
      currentSpeed *= @deceleration
    slowingDistance + (3*currentSpeed)



exports.SpeedDaemon = SpeedDaemon

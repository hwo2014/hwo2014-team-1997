net        = require "net"
JSONStream = require 'JSONStream'
fs         = require 'fs'
message    = require "./messageprocessor"
messages   = require "./messages"

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

logfile = fs.createWriteStream("racelog.json")
logfile.write "["

console.log "I'm #{botName} and connect to #{serverHost}: #{serverPort}"

client = net.connect serverPort, serverHost, () ->
  send(messages.advancedJoin(botName, botKey))

send = (json) ->
  logJSON json
  client.write JSON.stringify(json)
  client.write '\n'

end = (finishCallback) ->
  logfile.write "]"
  logfile.end()
  logfile.on 'finish', finishCallback if finishCallback?

writePasses = (track) ->
  #outfile = fs.createWriteStream("logs/" + Date.now() + "track.json")
  #outfile.write JSON.stringify(track)
  #outfile.end()
logJSON = (data) ->
  logfile.write JSON.stringify(data)
  logfile.write ',\n'

log = (msg) ->
  logfile.write "\"#{msg}\""
  logfile.write ',\n'

processor = new message.Processor
jsonStream = client.pipe(JSONStream.parse())
jsonStream.on 'data', (data) ->
  logJSON data
  try
    send processor.getResponse data
  catch ex
    console.error "Caught exception: #{ex}"
    end()
    client.end()
    writePasses processor.track
    if ex == "end"
      process.exit(0)
    else
      throw ex

jsonStream.on 'error', (err) ->
  console.log "disconnected: #{err}"
  logJSON err
  end()

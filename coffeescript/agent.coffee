class Agent
  constructor: () ->
    @HIGH_PRIORITY=3
    @MED_PRIORITY=2
    @LOW_PRIORITY=1
  act: (car, track, callback, result) ->
    callback?(result)
    result
  update: (track) ->
    
exports.Agent=Agent

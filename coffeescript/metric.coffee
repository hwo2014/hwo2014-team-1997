class Metric
  constructor: (value) ->
    @value = value if value?
    @lastValue = null
    @speed = null
    @lastSpeed = null
    @acceleration = null
    @lastAcceleration = null
    @jerk = null
  update: (newValue, ticks=1) ->
    @lastValue = @value
    @value = newValue
    @lastSpeed = @speed
    @speed = (@value - @lastValue) / ticks
    @lastAcceleration = @acceleration
    @acceleration = (@speed - @lastSpeed) / ticks
    @jerk = (@acceleration - @lastAcceleration) / ticks
  reset: (newValue) ->
    @value = newValue

exports.Metric = Metric


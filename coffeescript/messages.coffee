#Constants
exports.LEFT = "Left"
exports.RIGHT = "Right"

assert = (test, err) ->
  throw err if test != true

message = (name, data, ticks) ->
  result =
    msgType: name
  result.data = data if data?
  result.gameTick = ticks if ticks?

  return result
simpleJoin = (botName, botKey) ->
  message "join",
    name: botName
    key: botKey

advancedJoin = (botName, botKey, track = "keimola", count = 1) ->
  message "joinRace",
    botId:
      name: botName
      key: botKey
    trackName: track
    password: "stupidPassword"
    carCount: count

throttle = (value, ticks) ->
  value = parseFloat value
  assert(0.0 <= value <= 1.0, "Invalid throttle setting: #{value}")
  message "throttle", value, ticks

switchLane = (direction, ticks) ->
  assert(direction in [exports.LEFT, exports.RIGHT], "Invalid lane switch direction: #{direction}")
  message "switchLane", direction, ticks

turbo = (text="", ticks=undefined) ->
  message "turbo", text, ticks

ping = (ticks=undefined) ->
  message "ping", undefined, ticks

exports.simpleJoin = simpleJoin
exports.advancedJoin = advancedJoin
exports.throttle = throttle
exports.switchLane = switchLane
exports.turbo = turbo
exports.ping = ping
